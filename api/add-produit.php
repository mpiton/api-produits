<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST, GET");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../config/Database.php';
    include_once '../class/Produit.php';

    $database = new Database();
    $db = $database->getConnection();

    $item = new Produit($db);

    $data = json_decode(file_get_contents("php://input"));

    $item->nom = $data->prod_nom;
    $item->prix = $data->prod_prix;
    $item->categorie = $data->prod_cat;
    $item->contact = $data->prod_contact;
    
    if($item->createProduit()){
         http_response_code(201);
        echo 'Produit ajouté à la DB.';

    } else{
        echo 'Le produit ne peut être créé.';
    }
?>