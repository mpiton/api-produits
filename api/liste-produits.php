<?php
    header("Access-Control-Allow-Origin: *");
   header("Content-Type: application/json; charset=UTF-8");
    
    include_once '../config/Database.php';
    include_once '../class/Produit.php';

    $database = new Database();
    $db = $database->getConnection();

    $produit = new Produit($db);

    $stmt = $produit->getProduits();
    $produitCount = $stmt->rowCount();


    if($produitCount > 0){
        
        $produitArr = array();
        $produitArr["produit"] = array();
        $produitArr["produitCount"] = $produitCount;

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
          
            extract($row);
            $e = array(
                "prod_id" => $row['prod_id'],
                "prod_nom" => $row['prod_nom'],
                "prod_prix" => $row['prod_prix'],
                "prod_cat" => $row['prod_cat'],
                "prod_contact" => $row['prod_contact'],
            );
            array_push($produitArr["produit"], $e);
        }
        http_response_code(200);
        echo json_encode($produitArr);
    }

    else{
        http_response_code(404);
        echo json_encode(
            array("message" => "Rien dans la table.")
        );
    }
?>