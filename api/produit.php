<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../config/Database.php';
    include_once '../class/Produit.php';

    $database = new Database();
    $db = $database->getConnection();

    $item = new Produit($db);

    $item->id = isset($_GET['id']) ? $_GET['id'] : die();
  
    $item->getProduitById();

    if($item->nom != null){
        // create array
        $emp_arr = array(
            "prod_id" =>  $item->id,
            "prod_nom" => $item->nom,
            "prod_prix" => $item->prix,
            "prod_categorie" => $item->categorie,
            "prod_contact" => $item->contact,
        );
      
        http_response_code(200);
        echo json_encode($emp_arr);
    }
      
    else{
        http_response_code(404);
        echo json_encode("Produit non trouvée");
    }
?>