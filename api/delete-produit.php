<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: GET, POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once '../config/Database.php';
    include_once '../class/Produit.php';
    
    $database = new Database();
    $db = $database->getConnection();
    
    $item = new Produit($db);
    
    $data = json_decode(file_get_contents("php://input"));
    $item->id = $data->prod_id;
    
    if($item->deleteProduit()){
         http_response_code(200);
        echo json_encode("Produit supprime");
    } else{
        http_response_code(404);
        echo json_encode("Le produit n'a pas pu être supprime");
    }
?>