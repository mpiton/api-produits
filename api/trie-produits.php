<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../config/Database.php';
    include_once '../class/Produit.php';

    $database = new Database();
    $db = $database->getConnection();

    $item = new Produit($db);

    $item->prixmin = isset($_GET['min']) ? $_GET['min'] : die();
    $item->prixmax = isset($_GET['max']) ? $_GET['max'] : die();

    if ($item->TrieProduits() != null) {
       foreach ($item->TrieProduits() as $produit) {
       echo json_encode($produit);
    } 
    http_response_code(200);
    }
     else{
         http_response_code(404);
         echo json_encode("Les produits n'ont pas pu etre trouvee");
     }
?>