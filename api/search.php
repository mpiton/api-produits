<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: GET");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../config/Database.php';
    include_once '../class/Produit.php';

    $database = new Database();
    $db = $database->getConnection();

    $item = new Produit($db);

    $item->nom = isset($_GET['nom']) ? $_GET['nom'] : die();

    if ($item->SearchProduits() != null) {
       foreach ($item->SearchProduits() as $produit) {
       echo json_encode($produit);
    } 
    http_response_code(200);
    }
     else{
         http_response_code(404);
         echo json_encode("La recherche n'a pas pu aboutir.");
     }
?>