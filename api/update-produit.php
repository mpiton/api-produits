<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: PUT");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once '../config/Database.php';
    include_once '../class/Produit.php';
    
    $database = new Database();
    $db = $database->getConnection();
    var_dump($_SERVER);
    $item = new Produit($db);
    
    $data = json_decode(file_get_contents("php://input"));
    

    $item->id = isset($_GET['id']) ? $_GET['id'] : die();
   //  $item->id = $data->prod_id;
    
    $item->nom = $data->prod_nom;
    $item->prix = $data->prod_prix;
    $item->categorie = $data->prod_cat;
    $item->contact = $data->prod_contact;
    
    if($item->updateProduit()){
      http_response_code(200);
        echo json_encode("Produit mis à jour.");
    } else{ 
        echo json_encode("Les donnée n'ont pas pu etre mise à jour.");
    }
?>