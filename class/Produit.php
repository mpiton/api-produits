<?php 

class Produit
{
   //connexion
   private $conn;

   //Table
   private $db_table = "produit";

           // Champs
           public $id;
           public $nom;
           public $prix;
           public $categorie;
           public $contact;
           public $prixmin;
           public $prixmax;
           public $searchProd;
   
   //API KEY
   private $apiKey = "azerty";

                   // Info de connexion à la DB
        public function __construct($db){
         $this->conn = $db;
     }
             // Liste des produits (GET *)
             public function getProduits(){
               $sqlQuery = "SELECT prod_id, prod_nom, prod_prix, prod_cat, prod_contact FROM " . $this->db_table . "";
               $stmt = $this->conn->prepare($sqlQuery);
               $stmt->execute();
               return $stmt;
           }

           //Création (POST)
           public function createProduit() {
              if (isset($_GET["apikey"])) {
                 if ($_GET["apikey"] == $this->apiKey) {
                  $sqlQuery = "INSERT INTO
                  ". $this->db_table ."
                  SET
                  prod_nom = :nom, 
                  prod_prix = :prix, 
                  prod_cat = :categorie, 
                  prod_contact = :contact";
      
                  $stmt = $this->conn->prepare($sqlQuery);
      
                  // sanitize
                  $this->nom=htmlspecialchars(strip_tags($this->nom));
                  $this->prix=htmlspecialchars(strip_tags($this->prix));
                  $this->categorie=htmlspecialchars(strip_tags($this->categorie));
                  $this->contact=htmlspecialchars(strip_tags($this->contact));
      
                  // bind data
                  $stmt->bindParam(":nom", $this->nom);
                  $stmt->bindParam(":prix", $this->prix);
                  $stmt->bindParam(":categorie", $this->categorie);
                  $stmt->bindParam(":contact", $this->contact);
      
      
                  if($stmt->execute()){
                     return true;
                  }
                  return false;
                 }
              } else {
                 die();
              }
           }

           // Afficher un produit by ID (GET)
           public function getProduitById() {
            $sqlQuery = "SELECT
            prod_id,
            prod_nom,
            prod_prix,
            prod_cat,
            prod_contact
               FROM
                  ". $this->db_table ."
            WHERE 
               prod_id = ?
            LIMIT 0,1";

            $stmt = $this->conn->prepare($sqlQuery);

            $stmt->bindParam(1, $this->id);

            $stmt->execute();

            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->nom = $dataRow['prod_nom'];
            $this->prix = $dataRow['prod_prix'];
            $this->categorie = $dataRow['prod_cat'];
            $this->contact = $dataRow['prod_contact'];
           }

           // Suppression d'un produit (DELETE)
           public function deleteProduit(){
              if (isset($_GET["apikey"]) && ($_SERVER['REQUEST_METHOD'] == 'DELETE')) {
                 if ($_GET["apikey"] == $this->apiKey) {
                  $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE prod_id = ?";
                  $stmt = $this->conn->prepare($sqlQuery);
              
                  $this->id=htmlspecialchars(strip_tags($this->id));
              
                  $stmt->bindParam(1, $this->id);
              
                  if($stmt->execute()){
                      return true;
                  }
                  return false;
                 }
              } else {
                 die();
              }
           }

           // Update d'un produit par ID
           public function updateProduit(){
            if (isset($_GET["apikey"])) {
               if ($_GET["apikey"] == $this->apiKey) {
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                    prod_nom = :nom,
                    prod_prix = :prix,
                    prod_cat = :categorie,
                    prod_contact = :contact
                    WHERE 
                        prod_id = :id";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            //sanitize
            $this->nom=htmlspecialchars(strip_tags($this->nom));
            $this->prix=htmlspecialchars(strip_tags($this->prix));
            $this->categorie=htmlspecialchars(strip_tags($this->categorie));
            $this->contact=htmlspecialchars(strip_tags($this->contact));
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            // bind data
            $stmt->bindParam(":nom", $this->nom);
            $stmt->bindParam(":prix", $this->prix);
            $stmt->bindParam(":categorie", $this->categorie);
            $stmt->bindParam(":contact", $this->contact);
            $stmt->bindParam(":id", $this->id);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }
      } else {
            die();
         }
      }


           // SELECT `prod_nom`, `prod_prix` FROM produit WHERE `prod_prix` BETWEEN 10 AND 20 ORDER BY `prod_prix`
           //Entre min et max
           public function TrieProduits(){
            $sqlQuery = "SELECT
            prod_id,
            prod_nom,
            prod_prix,
            prod_cat,
            prod_contact
               FROM
                  ". $this->db_table ."
            WHERE
               prod_prix
            BETWEEN :min AND :max
            ORDER BY prod_prix";

            $stmt = $this->conn->prepare($sqlQuery);

            //sanitize
            $this->prixmin=htmlspecialchars(strip_tags($this->prixmin));
            $this->prixmax=htmlspecialchars(strip_tags($this->prixmax));

            //bind data
            $stmt->bindParam(':min', $this->prixmin);
            $stmt->bindParam(':max', $this->prixmax);

            $stmt->execute();

            $dataRow = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $dataRow;
           }

           //Recherche par nom
           public function SearchProduits(){
               $sqlQuery = "SELECT
               prod_id,
               prod_nom,
               prod_prix,
               prod_cat,
               prod_contact
                  FROM
                     ". $this->db_table ."
               WHERE 
                  prod_nom = ?
               ";
   
               $stmt = $this->conn->prepare($sqlQuery);
   
               $stmt->bindParam(1, $this->nom);
   
               $stmt->execute();
   
               $dataRow = $stmt->fetchAll(PDO::FETCH_ASSOC);
   
               return $dataRow;
         }	
}
?>